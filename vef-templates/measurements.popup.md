### {layer.title}
| | |
|--|--|
| Current Velocity | {properties.magnitude} {properties.unit} |
| Current Direction | {concatIfDefined(properties.direction_deg, " °")} |
| {isDefined(properties.parameter_name, properties.parameter_name, "Value")} | {properties.parameter_value} {properties.parameter_unit} |
| {isDefined(properties.date_time_end, "Date Time Start", "Date Time")} | {formatDate(properties.date_time_start)} |
| "Date Time End" | {formatDate(properties.date_time_end)} |
| Expedition | [{properties.expedition}](https://marine-data.de/?site=expedition&expedition={encodeURLComponent(properties.expedition)}") |
| Event | [{properties.event}](https://marine-data.de/?site=data&qf=events.name/{encodeURLComponent(properties.event)}")
| Device | {properties.device} |
| Method | {properties.method} |
| DOI | {concatIfDefined("<", properties.doi, ">")} |
