{marehubVideos()}
{marehubPhotos()}
### Space & Time
| | |
|--|--|
| {isDefined(properties.date_time_end, "Date Time Start", "Date Time")} | {formatDate(properties.date_time_start)} |
| Date Time End | {formatDate(properties.date_time_end)} |
| Coordinates | {formatGeometry(geometry)} |
| Elevation | {concatIfDefined(properties.elevation, " m")} |
| HEIGHT above seafloor | {concatIfDefined(properties["spanti.height_above_seafloor"], " m")} |
| Marine Zone | {properties["spanti.marine_zone"]} |
| Navigation | {properties["spanti.navigation"]} |
| {isDefined(properties.depth_type, properties.depth_type, "Depth")} | {concatIfDefined(properties.depth, " m")} |

### Event
| | |
|--|--|
| Event | [{properties.event}](https://marine-data.de/?site=data&qf=events.name/{encodeURLComponent(properties.event)}")
| Expedition | [{properties.expedition}](https://marine-data.de/?site=expedition&expedition={encodeURLComponent(properties.expedition)}") |
| Platform | {properties.platform} |
| Device | {properties.device} |
| Sensor | {properties.sensor} |
| Deployment | {properties.deployment} |

### General
| | |
|--|--|
| Format | {properties["general.format"]} |
| Filename | {properties["general.name"]} |
| Duration | {properties["general.duration"]} |
| File Size | {properties["general.file_size"]} {properties["general.file_size_unit"]} |
| Capture Mode | {properties["general.capture_mode"]} |
| Illumination | {properties["general.illumination"]} |
| Image Area | {properties["general.image_area"]} {properties["general.image_area_unit"]} |
| Capture Mode | {properties["general.capture_mode"]} |
| Image Quality | {properties["general.image_quality"]} |
| Pixel Density | {properties["general.pixel_density"]} {properties["general.pixel_density_unit"]} |
| Image Spectral Resolution | {properties["general.image_spectral_resolution"]} |
| Pixel Magnitude | {properties["general.pixel_magnitude"]} |
| Scale Reference | {properties["general.scale_reference"]} |

### Camera
| | |
|--|--|
| Brand | {properties["camera.brand"]} |
| Model | {properties["camera.model"]} |
| Lens Model | {properties["camera.lens_model"]} |
| Exposure Time | {properties["camera.exposure_time"]} {properties["camera.exposure_time_unit"]} |
| Focal Length | {properties["camera.focal_length"]} {properties["camera.focal_length_unit"]} |
| f-number | {properties["camera.f-number"]} |
| ISO Speed Ratings | {properties["camera.iso_speed_ratings"]} |

### Image
| | |
|--|--|
| Bit Depth | {properties["image.bit_depth"]} {properties["image.bit_depth_unit"]} |
| Chroma Subsampling | {properties["image.chroma_subsampling"]} |
| Color Space | {properties["image.color_space"]} |
| Resolution | {concatIfDefined(properties["image.width"], " x ", properties["image.height"], " px")} |
| Compression Mode | {properties["image.compression_mode"]} |

### Video
| | |
|--|--|
| Codec Name | {properties["video.codec_name"]} |
| Resolution | {concatIfDefined(properties["video.width"], " x ", properties["video.height"], " px")} |
| Frame Rate | {properties["video.frame_rate"]} {properties["video.frame_rate_unit"]} |
| Bit Rate | {properties["video.bit_rate"]} {properties["video.bit_rate_unit"]} |
| Color Space | {properties["video.color_space"]} |
| Pixel Format | {properties["video.pixel_format"]} |
| Scan Type | {properties["video.scan_type"]} |
| Color Model | {properties["video.color_model"]} |
| Chroma Subsampling | {properties["video.chroma_subsampling"]} |
| Display Aspect Ratio | {properties["video.display_aspect_ratio"]} |

### Audio
| | |
|--|--|
| Codec Name | {properties["audio.codec_name"]} |
| Sample Rate | {properties["audio.sampling_rate"]} {properties["audio.sampling_rate_unit"]} |
| Sample Format | {properties["audio.sample_format"]} |
| Channels | {properties["audio.channels"]} |
| Compression Mode | {properties["audio.compression_mode"]} |
| Bit Rate | {properties["audio.bit_rate"]} {properties["audio.bit_rate_unit"]} |
| Bit Depth | {properties["audio.bit_depth"]} {properties["audio.bit_depth_unit"]} |

### References
| | |
|--|--|
| License | {properties.license} |
| Data URL | {concatIfDefined("<", properties.data_url, ">")}
| Dataset | {properties.dataset} |
| DOI | {concatIfDefined("<", properties.doi, ">")}
| Metadata URL | {concatIfDefined("<", properties.metadata_url, ">")}
| Provider | {properties.provider} |
| Copyright | {properties["references.copyright"]} |
| Principal Investigator | {properties["references.pi_name"]} |
| PI ORCID | {properties["references.pi_orcid"]} |
