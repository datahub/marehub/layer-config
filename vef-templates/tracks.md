### Expedition {properties.expedition}
| | |
|--|--|
| Platform | {properties.platform} |
| Expedition | [{properties.expedition}](https://marine-data.de/?site=expedition&expedition={properties.expedition}") |
| Alias | {properties.expedition_alias} |
| Begin | {formatDate(properties.begin_date)} |
| End | {formatDate(properties.end_date)} |
