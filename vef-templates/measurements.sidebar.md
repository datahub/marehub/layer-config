### Space & Time
| | |
|--|--|
| {isDefined(properties.date_time_end, "Date Time Start", "Date Time")} | {formatDate(properties.date_time_start)} |
| Date Time End | {formatDate(properties.date_time_end)} |
| Coordinates | {formatGeometry(geometry)} |
| Elevation | {concatIfDefined(properties.elevation, " m")} |
| {isDefined(properties.depth_type, properties.depth_type, "Depth")} | {concatIfDefined(properties.depth, " m")} |

### Event
| | |
|--|--|
| Expedition | [{properties.expedition}](https://marine-data.de/?site=expedition&expedition={encodeURLComponent(properties.expedition)}") |
| Expedition Alias | {properties.expedition_alias} |
| Event | [{properties.event}](https://marine-data.de/?site=data&qf=events.name/{encodeURLComponent(properties.event)}") |
| Event Alias | {properties.event_alias} |
| Platform | {properties.platform} |
| Device | {properties.device} |

### Data
| | |
|--|--|
| Current Velocity | {properties.magnitude} {properties.unit} |
| Current Direction | {concatIfDefined(properties.direction_deg, " °")} |
| {isDefined(properties.parameter_name, properties.parameter_name, "Value")} | {properties.parameter_value} {properties.parameter_unit} |
| Method | {properties.method} |
| Original Parameter Name | {properties.parameter_origin_name} |
| Original Unit | {properties.parameter_origin_unit} |
| Original Method | {properties.origin_method} |

### References
| | |
|--|--|
| License | {properties.license} |
| Sensor URI | {concatIfDefined("<", properties.sensor_uri, ">")}
| DOI | {concatIfDefined("<", properties.doi, ">")}
| Data URL | {concatIfDefined("<", properties.data_url, ">")}
| Metadata URL |{concatIfDefined("<", properties.metadata_url, ">")}
| Provider | {properties.provider} |
| Dataset | {properties.dataset} |
| Curator | {properties.curator} |
