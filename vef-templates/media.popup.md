### {properties["hidden.media_type"]} {concatIfDefined(" | ", properties["general.format"])}
| | |
|--|--|
| {isDefined(properties.date_time_end, "Date Time Start", "Date Time")} | {formatDate(properties.date_time_start)} |
| Event | [{properties.event}](https://marine-data.de/?site=data&qf=events.name/{encodeURLComponent(properties.event)}")
| Device | {properties.device} |
| Camera Model | {properties["camera.model"]} |
| Lens Model | {properties["camera.lens_model"]} |
| Resolution | {concatIfDefined(properties["image.width"], " x ", properties["image.height"], " px")} |
| Resolution | {concatIfDefined(properties["video.width"], " x ", properties["video.height"], " px")} |
| Codec Name | {properties["video.codec_name"]} |
| Frame Rate | {properties["video.frame_rate"]} {properties["video.frame_rate_unit"]} |
