### Event {properties.event}
| | |
|--|--|
| Device | {properties.device} |
| Platform | {properties.platform} |
| Date | {formatDate(properties.begin_date)} |
| Begin Coordinates | {formatLatLng(properties.begin_latitude, properties.begin_longitude)} |
| Expedition | [{properties.expedition}](https://marine-data.de/?site=expedition&expedition={properties.expedition}") |
| Event | [{properties.event}](https://marine-data.de/?site=data&qf=events.name/{properties.event}") |
